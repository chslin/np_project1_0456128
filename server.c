#include <sys/types.h>
#include <sys/signal.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/errno.h>
#include <sys/resource.h>

#include <unistd.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#define QLEN  32
#define BUFSIZE 50000

extern int errno;


struct sarray{
	char *arg[100];
	int endnum;//在第幾個token結束
	int size;
	int pipefd[2];//橫向
};
struct tab{
	int connect;
	int pipefd[2];//縱向
};
void unknown(int line_num ,struct tab *table);
void reaper(int sig);
int TCPpassing(int sockfd);
int passiveTCP(char *service,int qlen);
int passivesock(char *,char *,int);
int readline(int fd,char *ptr,int maxlen);
int writefile(char *filename);
void token(char *,int,int,struct tab *,int *tabnum);
void createpipe(int k,struct sarray *stra);
int searchtable(int countline,struct tab *table);
int updatetable(int check,int *tabnum,struct tab *table);
struct tab usetable(int index,struct tab *table);
int countline;
int pipecount=0;
/*
Unix系統裡，每行結尾只有“<換行>”，即“\n”；Windows系統裡面，每行結尾是“<換行><回車>”，即“\n\r”；
Mac系統裡，每行結尾是“<回車>”。一個直接後果是，Unix/Mac系統下的文件在Windows裡打開的話，所有文字會變成一行；
而Windows裡的文件在Unix/Mac下打開的話，在每行的結尾可能會多出一個^M符號。
*/
char *delin=" \n\r";             //linux 下一行

main(int argc,char *argv[])
{
	char *service = "7000";
	struct sockaddr_in fsin;
	int alen;
	int msock;
	int ssock;
	
	switch(argc){
	case 1:
		break;
	case 2:
		service=argv[1];
		break;
	default:
		exit(1);
	}
	msock=passiveTCP(service,QLEN);
	
	chdir("/u/gcs/104/0456128/project1/ras/");
	setenv("PATH","bin:.",1);
	signal(SIGCHLD,reaper);
	printf("socket success\n");
	while(1){
		alen=sizeof(fsin);
		ssock=accept(msock,(struct sockaddr *)&fsin,&alen);
		if(ssock<0){
			if(errno==EINTR)
				continue;
			exit(1);
		}
		switch(fork()){
		case 0:			//child
			close(msock);
			printf("child\n");
			write(ssock,"****************************************\n** Welcome to the information server. **\n****************************************  \n% ",127);
			
			exit(TCPpassing(ssock));
		default:		//parent
			close(ssock);
			break;
		case -1:
			printf("fork error");
			exit(1);
		}
	}
}

void reaper(int sig)
{
        int status;
        while(wait3(&status, WNOHANG, (struct rusage *)0) >= 0);
}
int passiveTCP(char *service,int qlen)
{
	return passivesock(service,"TCP",qlen);
}
int passivesock(char *service,char *protocal,int qlen)
{
	
	struct protoent *ppe;
	struct sockaddr_in sin;
	int sockfd,type;
	int po = atoi( service);
	
	bzero((char *)&sin,sizeof(sin));
	sin.sin_family=AF_INET;
	sin.sin_addr.s_addr=htonl(INADDR_ANY);
	
	sin.sin_port=htons(po);
	
	if((ppe=getprotobyname(protocal))==0){
		//errexit("usage:connect server error");;
		printf("portname error\n");
		exit(1);
	}
	if(strcmp(protocal,"udp")==0)
		{
		type=SOCK_DGRAM;}
	else{
	
		type=SOCK_STREAM;}
	sockfd=socket(PF_INET,type,ppe->p_proto);
	if(sockfd<0){
		//errexit("usage:connect server error");
		exit(1);
	}
	if(bind(sockfd,(struct sockaddr *)&sin,sizeof(sin))<0){
		//errexit("usage:connect server error");
		exit(1);
	}
	if(type==SOCK_STREAM&&listen(sockfd,qlen)<0){
		//errexit("usage:connect server error");
		printf("listen fail\n");
		exit(1);
	}
	printf("rerurn sockfd\n");
	return sockfd;
}
int TCPpassing(int sockfd)
{
	int n;
	//int outfd;
	char line[BUFSIZE];
	struct tab table[4999];
	int tabnum=0;
	//int update=0;
	countline=0;
	for(;;){
		//pipecount=0;
		bzero(line,BUFSIZE);
		n=readline(sockfd,line,BUFSIZE);
		countline++;
		printf("%d%s\n", countline,line);
		if(n==0) return 0;
		else if(n<0){ //errexit("readline error");
			printf("readline error\n");
			exit(1);
		}
		token(line,sockfd,countline,table,&tabnum);
		write(sockfd,"% ",2);

	}
	return 0;
}

void token(char *s,int fd,int countline,struct tab *table,int *tabnum)
{
	//s:讀進的字串  fd:sockfd 
	
	struct tab tail,head; 
	int uptateindex=0;
	struct sarray stra[8000];
	int check;
	int count=0;
	int segment=0;
	int k,l,r,f;
	int pid,pid1;
	char *pipenum;
	char *temp;
	char *a[7000];
	long i=0;
	temp=strtok(s,delin);
	while(temp!=NULL)
	{
		//將讀進的字串作分割 以delin作區分  存進a[i]
		a[i]=temp;
		temp=strtok(NULL,delin);
		i++;
	}
		//判斷a[0]字首
	if(strcmp(a[0],"exit")==0){
		exit(0);
	}
	if(strcmp(a[0],"setenv")==0){
		setenv(a[1],a[2],1);
	}
	else if(strcmp(a[0],"printenv")==0){
		if(fork()==0){
			close(1);
			dup(fd);
			printf("PATH=%s\n",getenv("PATH"));
			fflush(stdout);
			exit(0);
		}
		else	
			wait(NULL);
	}
	else{
		for(k=l=0;k<i;k++,l++)
		{
			//判斷讀進的字串是否有 > | !
			if((*(a[k])!='>') && (*(a[k])!='|') && (*(a[k])!='!')){
				count++;
				stra[segment].arg[l]=a[k];
				stra[segment].size=count;
				stra[segment].endnum=0;
				if(k==(i-1)){
					stra[segment].arg[l+1]=0;
				}
			}
			else {	
				stra[segment].endnum=k;
				stra[segment].arg[l]=0;
				if(strlen(a[k])==1){
					segment++;
					l=-1;
					count=0;
				}			
			}
		}
		if(((*(a[stra[segment].endnum])=='|')||(*(a[stra[segment].endnum])=='!'))&&(strlen(a[stra[segment].endnum])>1)){
		
			//r=searchtable(countline,table);
			if((*(a[stra[segment].endnum])=='|')){
				pipenum=strtok(a[stra[segment].endnum],"|");
			}
			else{ 
				pipenum=strtok(a[stra[segment].endnum],"!");
			}
			check=countline+atoi(pipenum);
			
			if(f=searchtable(check,table)<0){
				uptateindex=updatetable(check,tabnum,table);
				tail=usetable(uptateindex,table);
			}
			else tail=usetable(f,table);
		}
		r=searchtable(countline,table);
		if(r>=0){
			head=usetable(r,table);
			close(head.pipefd[1]);
		}
		if((pid1=fork())<0){perror("Fork_error!\n");}
		else if(pid1==0){
			if(r>=0){
				close(0);
				dup(head.pipefd[0]);
				close(head.pipefd[0]);
				close(head.pipefd[1]);
			}
			for(k=0;k<=segment;k++){
				if((*(a[stra[k].endnum])=='|') && (strlen(a[stra[k].endnum])==1)){
					createpipe(k,stra);
					
					switch(fork()){
						case 0:				//child
							close(1);
							dup(stra[k].pipefd[1]);
							close(2);
							dup(fd);
							close(stra[k].pipefd[1]);
							close(stra[k].pipefd[0]);
							if(execvp(stra[k].arg[0],stra[k].arg)){
								close(1);
								dup(fd);
								printf("Unknow command:[%s]\n",stra[k].arg[0]);
								fflush(stdout);
								exit(0);
							}
							break;
						default:			//parent
							close(0);
							dup(stra[k].pipefd[0]);
							close(stra[k].pipefd[1]);
							close(stra[k].pipefd[0]);
							wait(NULL);
							break;
						case -1:			//fork error
							perror("Fork_error!\n");
							exit(0);
					}
				}
				else if((*(a[stra[k].endnum])=='!') && (strlen(a[stra[k].endnum])==1)){
					createpipe(k,stra);
					//pipecount++;
					switch(fork()){
						case 0:					//child
							close(1);
							dup(stra[k].pipefd[1]);
							close(2);
							dup(stra[k].pipefd[1]);
							close(stra[k].pipefd[1]);
							close(stra[k].pipefd[0]);
							if(execvp(stra[k].arg[0],stra[k].arg)){
							close(1);
							dup(fd);
							printf("Unknow command:[%s]\n",stra[k].arg[0]);
							fflush(stdout);
							exit(0);
							}
							break;
							
						default:				//parent
							close(0);
							dup(stra[k].pipefd[0]);
							//close(2);
							//dup(parray[pipecount].pipefd[0]);
							close(stra[k].pipefd[1]);
							close(stra[k].pipefd[0]);
							wait(NULL);
							break;
						
						case -1:				//fork error
							perror("Fork_error!\n");
							exit(0);
					}				
				}
				else if((*(a[stra[k].endnum])=='|') && (strlen(a[stra[k].endnum])>1)){
					switch(fork()){
						case 0:
							close(1);
							dup(tail.pipefd[1]);
							close(2);
							dup(fd);
							close(tail.pipefd[1]);
							close(tail.pipefd[0]);
							if(execvp(stra[k].arg[0],stra[k].arg)){
								close(1);
								dup(fd);
								printf("Unknow command:[%s]\n",stra[k].arg[0]);
								fflush(stdout);
								exit(0);
							}
							break;
							
						default:
							wait(NULL);
							break;
						case -1:
							perror("Fork_error!\n");
							exit(0);
					}
										
				}
				else if((*(a[stra[k].endnum])=='!') && (strlen(a[stra[k].endnum])>1)){
					switch(fork()){
						case 0:				//child
							close(1);
							dup(tail.pipefd[1]);
							close(2);
							dup(tail.pipefd[1]);
							close(tail.pipefd[1]);
							close(tail.pipefd[0]);
							if(execvp(stra[k].arg[0],stra[k].arg)){
								close(1);
								dup(fd);
								printf("Unknow command:[%s]\n",stra[k].arg[0]);
								fflush(stdout);
								exit(0);
							}
							break;
						default:			//parent
							wait(NULL);
							break;
						case -1:			//fork error
							perror("Fork_error!\n");
							exit(0);
					}
				}
				else if((*(a[stra[k].endnum])=='>') && (strlen(a[stra[k].endnum])==1)){
					createpipe(k,stra);
					//pipecount++;
					switch(fork()){
						case 0:				//child
							close(1);
							dup(stra[k].pipefd[1]);
							close(2);
							dup(fd);
							close(stra[k].pipefd[1]);
							close(stra[k].pipefd[0]);
							if(execvp(stra[k].arg[0],stra[k].arg)){
								close(1);
								dup(fd);
								printf("Unknow command:[%s]\n",stra[k].arg[0]);
								fflush(stdout);
								exit(0);
							}
							break;
						default:			//parent
							close(0);
							dup(stra[k].pipefd[0]);
							close(stra[k].pipefd[1]);
							close(stra[k].pipefd[0]);
							wait(NULL);
							writefile(stra[k+1].arg[0]);
							k++;
							break;
						case -1:			//fork error
							perror("Fork_error!\n");
							exit(0);
					}			
				}
				else{
					switch(fork()){
						case 0:				//child
							close(1);
							dup(fd);
							close(2);
							dup(fd);
							if(execvp(stra[k].arg[0],stra[k].arg)){
								close(1);
								dup(fd);
								printf("Unknow command:[%s]\n",stra[k].arg[0]);
								
								fflush(stdout);
								//unknown(countline,table);
								exit(0);
							}
							break;
						default:			//parent
							wait(NULL);
							break;
						case -1:			//fork error
							perror("Fork_error!\n");
							exit(0);
					}
								
				}		
			}	
			exit(0);
		}
		else{
			//close(tail.pipefd[1]);
			wait(NULL);
			if(r>=0){
				close(head.pipefd[0]);
			}
		}
	}	
}

int writefile(char *filename)
{
	int numbytes;
	FILE *fp;

	char buff[BUFSIZE];

	if ( (fp = fopen(filename, "wt")) == NULL )
			perror("can't open file");

	if ((numbytes = read(STDIN_FILENO, buff, sizeof(buff))) < 0)
		perror("Error");
	//fprintf(fp, "%s", buff);
	fputs(buff,fp);
	fclose(fp);
	return 0;
} 
void createpipe(int k,struct sarray *stra){

	/*
		使用 pipe() 可建立一組雙向的管線
		利用 dup2()，可以讓管線去取代外部程式的標準輸出（standard output），然後讓主程式用管線接收
	*/
	if(pipe(stra[k].pipefd)<0){
		perror("pipe error");
		exit(0);
	}
	//pipecount++;
}
struct tab usetable(int index,struct tab *table)
{
	return table[index];
}
int readline(int fd,char *ptr,int maxlen)
{
	int n,rc;
	char c;
	*ptr = 0;
	for(n=1;n<maxlen;n++)
	{
		if((rc=read(fd,&c,1)) == 1)
		{
			*ptr++ = c;	
			//if(c==' '&& *(ptr-2) =='%'){ break; }
			if(c=='\n')  break;
		}
		else if(rc==0)
		{
			if(n==1)     return(0);
			else         break;
		}
		else
			return(-1);
	}
	return(n);
} 
int searchtable(int countline,struct tab *table)					//利用countline在table[].connect尋找  找到回傳該connect之table
{
	int i;
	for(i=0;i<4999;i++){
		if (countline==table[i].connect){
			return i;
		}
	}
	return -1;
}
int updatetable(int check,int *tabnum,struct tab *table)						
{
	int i;
	i=(*tabnum);
	table[*tabnum].connect=check;
	if(pipe(table[*tabnum].pipefd)<0){
		perror("pipe error");
		exit(0);
	}
	//pipecount++;
	(*tabnum)++;
	return i;
}
